## SpringBoot + mica-mqtt 应用演示

## 步骤
1. 启动 MqttClientTest 和 MqttServerTest

2. 启动 MicaMqttApplication

3. 查看控制器 swagger 地址：http://localhost:30012/doc.html

4. 可开启 prometheus 指标收集，详见： http://localhost:30012/actuator/prometheus

## 连接

mica Spring boot 开发组件集文档：https://www.dreamlu.net/components/mica-swagger.html
